
Configuring WRF
===============

Settings for namelist.input in WRF
----------------------------------

A new group `&iceblade` is added to the WRF input namelist to control the iceBlade model.

.. code-block::

    &iceblade
        iceblade_opt = 1, 1, 1,
        num_turbs = 3,
        bld_diam = 0.03, 0.14, 0.14, 0.14
        adj_bld_ws = .true.,
    /


=============  ==================================  =======
Variable name  Default value                       Description
=============  ==================================  =======
iceblade_opt   1,1,1                               Activate (=1) iceBlade domain-wise
num_turbs      3                                   Number of turbines to run [1,2 or 3]
bld_diam       0.03, 0.14, 0.14, 0.14              Parameter 1: specifies the blade dimeter of a "fixed cylinder" (0.03 is ISO standard) 

                                                   Parameter 2-4: "leading edge diameter" for each turbine
adj_bld_ws     .true.                              Take into account the rotational speed of the turbine (.true. recommended)
=============  ==================================  =======

Turbine-file in WRF
-------------------
IceBlade by default simulates ice accretion and ice ablation on 4 objects: a standard rotating cylinder as well as three turbine types. The turbine types can be configured through a modified version of the WRF turbine file, which is used to parameterize the effect of wind farms (:cite:`Fitch2012`). Below the example turbine provided with the WRF source code in `run/wind-turbine1.tbl <https://gitlab.windenergy.dtu.dk/dtu-iceblade/wrf/-/blob/2ee847bf501792dae149c9c75bc89a6bc45435c1/run/wind-turbine_ice-1.tbl>`_ with those modification is shown: an additional column, the rotational speed in rotations per minute (RPM), is introduced. In the example below, hub height and (135 m) and rotor diameter (127 m) have also been changed from the default to correspond to the performed :ref:`case study <Verifying the iceBlade implementation>`. The example file is included in the repository at `run/wind-turbine1.tbl <https://gitlab.windenergy.dtu.dk/dtu-iceblade/wrf/-/blob/2ee847bf501792dae149c9c75bc89a6bc45435c1/run/wind-turbine_ice-1.tbl>`_. Note that for IceBlade the standing thrust coefficient (0.130) and nominal power of the turbine (2.0 MW) as well as the thrust curve (second column) and power production (third column) are not used.

.. code-block::

    23
    135. 127. 0.130 2.0
    3.   0.805    10.0  5.0
    4.   0.805    50.0  5.0
    5.   0.805   150.0  6.015
    6.   0.805   280.0  7.218
    7.   0.805   460.0  8.421
    8.   0.805   700.0  9.624
    9.   0.805   990.0  10.83
    10.  0.790  1300.0  12.03
    11.  0.740  1600.0  12.1
    12.  0.700  1850.0  12.1
    13.  0.400  1950.0  12.1
    14.  0.300  1990.0  12.1
    15.  0.250  1995.0  12.1   
    16.  0.200  2000.0  12.1
    17.  0.160  2000.0  12.1
    18.  0.140  2000.0  12.1  
    19.  0.120  2000.0  12.1
    20.  0.100  2000.0  12.1   
    21.  0.080  2000.0  12.1
    22.  0.070  2000.0  12.1
    23.  0.060  2000.0  12.1
    24.  0.055  2000.0  12.1
    25.  0.050  2000.0  12.1

Other physics schemes in WRF
----------------------------
The default implementation of the iceBlade model has been tuned to run with the Thompson microphysics scheme (8) or the Thompson Aerosol-Aware microphysics scheme (28), and the use of other schemes would likely require modification to iceBlade. For example, the WSM5 scheme, often used for speed, has been used in previous wind farm icing studies by combining the QCLOUD and QICE fields into 1 value for use in the calculation of cloud liquid water. However, it is not recommended for icing cases as it does not handle the partitioning of frozen and unfrozen hydrometeors as well as the Thompson scheme, and the shape of the cloud droplet distribution is not as well understood. All other WRF configuration options can be changed without modification to the iceBlade model.
