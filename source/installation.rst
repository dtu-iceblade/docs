Installation (WRF)
==================


Download WRF
------------

You can download the modified WRF source code from the DTU-gitlab::

    git clone https://gitlab.windenergy.dtu.dk/dtu-iceblade/wrf.git


Getting started in WRF
----------------------
To get started, please follow these steps 

.. toctree::
    :maxdepth: 1

    build
    configuration




Source code modifications in WRF
--------------------------------
The original WRF source code was changed at several places to include iceBlade. An overview over all changes can be obtained from

    https://gitlab.windenergy.dtu.dk/dtu-iceblade/wrf/-/compare/NCAR...master?from_project_id=2367

In summary these changes include:

1. The main iceBlade model is included in the `iceBlade module <https://gitlab.windenergy.dtu.dk/dtu-iceblade/wrf/-/blob/2ee847bf501792dae149c9c75bc89a6bc45435c1/phys/module_diag_iceblade.F>`_
2. To configure the iceBlade model a new `&iceblade`-group has been added to the `namelist.input <https://gitlab.windenergy.dtu.dk/dtu-iceblade/wrf/-/compare/NCAR...master?from_project_id=2367#c9dad068b94d89d2254ad562aa6fb1c1e07447ba>`_
3. An example `turbine file <https://gitlab.windenergy.dtu.dk/dtu-iceblade/wrf/-/compare/NCAR...master?from_project_id=2367#5e8df1df83d8e8787dbe5c872b38ed42e3679bee>`_ is explained in detail :ref:`here <Turbine-file in WRF>`