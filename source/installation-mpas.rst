Installation (MPAS)
===================


Download MPAS
---------------

MPAS-iceBlade can be obtained from DTU-gitlab::

    git clone https://gitlab.windenergy.dtu.dk/dtu-iceblade/mpas.git


Getting started in MPAS
-----------------------
To get started, please follow these steps 

.. toctree::
    :maxdepth: 1

    build-mpas
    configuration-mpas

Source code modifications in MPAS
---------------------------------

A detailed overview of the modifications to the official MPAS release is provided in the git repository by comparing `master to NCAR <https://gitlab.windenergy.dtu.dk/dtu-iceblade/mpas/-/compare/NCAR...master?from_project_id=2368>`_. 
These changes include: 

1. Creation of the new diagnostic module in `src/core_atmosphere/diagnostics/icing_diagnostics.F <https://gitlab.windenergy.dtu.dk/dtu-iceblade/mpas/-/blob/691c576b1253e9ed81bd50a9124f185f53ff6ca0/src/core_atmosphere/diagnostics/icing_diagnostics.F>`_ containing the hearth of the iceBlade model (initialization, computation, updating and resetting of the diagnostic variables)
2. Introduction of new Registry file (`src/core_atmosphere/diagnostics/Registry_ice.xml <https://gitlab.windenergy.dtu.dk/dtu-iceblade/mpas/-/blob/691c576b1253e9ed81bd50a9124f185f53ff6ca0/src/core_atmosphere/diagnostics/Registry_ice.xml>`_) defining the new diagnostics related to iceBlade
3. Minor modifications to `diagnostic manager <https://gitlab.windenergy.dtu.dk/dtu-iceblade/mpas/-/compare/NCAR...master?from_project_id=2368#d530f533d1487072f3ceb2620a4018e32329ab7f>`_ and root level `Registry <https://gitlab.windenergy.dtu.dk/dtu-iceblade/mpas/-/compare/NCAR...master?from_project_id=2368#61578b06c17e548925e7b5e2143ce6d0cfd0387a>`_ and `Makefile <https://gitlab.windenergy.dtu.dk/dtu-iceblade/mpas/-/compare/NCAR...master?from_project_id=2368#2284b2ebfdbdbaa9bad387edfbc3ef6df5ecdb47>`_ to accommodate new changes
