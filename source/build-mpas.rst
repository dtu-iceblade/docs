Building MPAS-iceBlade
======================

Once the source code has been cloned/downloaded from the git repository, it can be compiled like the official MPAS model.
Detailed build instructions can be found in the MPAS User Guide :cite:`Duda2019`.