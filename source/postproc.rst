Postprocessing (old)
====================

IceBlade provides accumulated accretion and ablation over each output time step for a standard rotating cylinder as well as for the three turbine types (:ref:`see here <Turbine-file in WRF>`)

.. code-block::

    ACCRE_CYL, ACCRE_BLD1, ACCRE_BLD2, ACCRE_BLD3
    ABLAT_CYL, ABLAT_BLD1, ABLAT_BLD2, ABLAT_BLD3

To derive the accumulated ice amount, ice accretion and ice ablation need to be combined:

.. code-block::

    import xarray as xr

    def ice_load(accre,ablat):
            load = accre.copy() * 0 
            for ihouroi,houroi in enumerate(accre.time):
                if houroi == accre.time.isel(time=0):
                    acm = load.sel(time=houroi)
                else:
                    acm = load.isel(time=ihouroi-1).copy()
                # Add ice when there is new accretion and remove when there is ablation
                acm += xr.where(accre.sel(time=houroi) > 0.0005, accre.sel(time=houroi), xr.where(ablat.sel(time=houroi) < 0., ablat.sel(time=houroi), 0.))
                # Ensure that ice never goes negative
                load.loc[{'time':houroi}] =  xr.where(acm < 0., 0., acm)
        return load


    load = ice_load(ds['ACCRE_CYL'].load(),ds['ABLAT_CYL'].load()).rename('ice_load')