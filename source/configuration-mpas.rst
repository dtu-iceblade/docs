
Configuring MPAS
================

Settings for namelist.input in MPAS
-----------------------------------

A new group `&iceblade` is added to the default atmosphere namelist to control the iceBlade model.

.. code-block::

    &iceblade
        config_ice_opt = 0
        config_num_turbs = 3
        config_bld_diam = 0.03
        config_adj_bld_ws = true
    /


====================  ==================================  =======
Variable name         Default value                       Description
====================  ==================================  =======
config_ice_opt        0                                   Activate (=1) / Deactivate (=0) iceBlade
config_num_turbs      3                                   Number of turbines to run [maximum 3]
config_bld_diam       0.03                                Diameter of a "fixed cylinder" (0.03 is ISO standard) 
config_adj_bld_ws     true                                Take into account the rotational speed of the turbine 
                                                          (true is recommended)
====================  ==================================  =======

Turbine-file in MPAS
--------------------
IceBlade by default simulates ice accretion and ice ablation on 4 objects: 
a standard rotating cylinder as well as three turbine types. 
The turbine types can be configured through a modified version of the WRF turbine file, 
commonly used to parameterize the effect of wind farms in WRF (:cite:`Fitch2012`).
Some modifications to the standard structure are needed and an example of the final file 
structure for MPAS-iceBlade is given below:

.. code-block::

    23
    135. 127. 0.130 2.0 0.14
    3.   0.805    10.0  5.0
    4.   0.805    50.0  5.0
    5.   0.805   150.0  6.015
    6.   0.805   280.0  7.218
    7.   0.805   460.0  8.421
    8.   0.805   700.0  9.624
    9.   0.805   990.0  10.83
    10.  0.790  1300.0  12.03
    11.  0.740  1600.0  12.1
    12.  0.700  1850.0  12.1
    13.  0.400  1950.0  12.1
    14.  0.300  1990.0  12.1
    15.  0.250  1995.0  12.1   
    16.  0.200  2000.0  12.1
    17.  0.160  2000.0  12.1
    18.  0.140  2000.0  12.1  
    19.  0.120  2000.0  12.1
    20.  0.100  2000.0  12.1   
    21.  0.080  2000.0  12.1
    22.  0.070  2000.0  12.1
    23.  0.060  2000.0  12.1
    24.  0.055  2000.0  12.1
    25.  0.050  2000.0  12.1
    
The modified file structure includes: 

1. An additional 4th column, the rotational speed of the turbine in rotations per minute (RPM) as function of wind speed, is introduced
2. An additional 5th entry in the second row, the leading edge diameter (in meters) used for the particular turbine is specified. :ref:`**NOTE: This specification of the leading edge diameter within the turbine file itself is different to WRF-iceBlade where leading edge diameter are communicated via namelist variables.** <Settings for namelist.input in WRF>`

Note that for IceBlade the standing thrust coefficient (0.130), nominal power of the turbine (2.0MW) 
as well as the thrust curve (2nd column) and power production (3rd column) are not used.

Physics settings in MPAS
------------------------

Currently, the implementation of iceBlade in MPAS relies on the usage 
of the Thompson (non-aerosol aware) microphysics scheme which is in line with the physics scheme the original 
iceBlade code has been tuned against. It is expected that 
the usage of other microphysics schemes would likely require modification to the model code. 

The Thompson microphysics scheme is part of MPAS' *'convection_permitting'* physics suite and while 
iceBlade does not set any limitations to the choice of other physics schemes (cumulus, radiation etc.),
it is suggested to start with the default *'convection_permitting'* suite, since
those physics scheme combinations have been tested together :cite:`Duda2019`. 

More details about the physics suites and available physics in MPAS can be found in the User Guide 
:cite:`Duda2019`.

