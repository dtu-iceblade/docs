# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import sys
# sys.path.insert(0, os.path.abspath('.'))

import os
import datetime

import errno
import sphinx.util.osutil
sphinx.util.osutil.ENOENT = errno.ENOENT


# -- Project information -----------------------------------------------------

project = 'iceBlade'
copyright = str(datetime.datetime.now().year) + u", DTU Wind Energy"
author = 'Neil Davis, Jana Fischereit, Marc Imberger'

# The full version, including alpha/beta/rc tags
release = '1.0'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions =['sphinxcontrib.bibtex',
             'sphinx.ext.autosectionlabel',
            "sphinx.ext.autodoc",
            "sphinx.ext.coverage",
            "sphinx.ext.imgmath",
            # Allow inclusion of dot graphics
            "sphinx.ext.graphviz",
            # Document across sphinx instances
            "sphinx.ext.intersphinx",
            # Use Numpy or Google docstrings
            "sphinx.ext.napoleon",
            # Add links to sourcecode
            "sphinx.ext.viewcode",
            # Create a table with documentation
            "sphinx.ext.autosummary",
            # Document command line tools
            "sphinxarg.ext",
            # tikz picutures
            'sphinxcontrib.tikz',
            "sphinx_rtd_theme"

]
bibtex_bibfiles = ['refs.bib']

tikz_proc_suite = 'GhostScript'
tikz_latex_preamble = '\\tikzset{ \
wind turbine/.pic={\
  \\tikzset{path/.style={fill, ultra thick, line join=round}}\
  \path [path] \
    (-.25,0) arc (180:360:.25 and .0625) -- (.0625,3) -- (-.0625,3) -- cycle;\
  \\foreach \i in {90, 210, 330}{\
      \path [path, shift=(90:3), rotate=\i] \
        (.5,-.1875) arc (270:90:.5 and .1875) arc (90:-90:1.5 and .1875);\
  }\
  \path [path] (0,3) circle [radius=.25];\
}\
} \
\\def\\currdir{' + os.path.abspath('.') +'}'

tikz_tikzlibraries = 'positioning'


# Custom configuration for autosummary
autosummary_generate = True
imported_members = True


# Custom configuration of coverage
coverage_skip_undoc_in_source = False
coverage_write_headline = True


# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'

# Theme options are theme-specific and customize the look and feel of a theme
# further.  For a list of options available for each theme, see the
# documentation.
html_theme_options = {
    # TOC options
    #'navigation_depth': 2,  # only show 2 levels on left sidebar
    'collapse_navigation': False,  # don't allow sidebar to collapse
}

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

# Custom sidebar templates, must be a dictionary that maps document names
# to template names.
#
# The default sidebars (for documents that don't match any pattern) are
# defined by theme itself.  Builtin themes are using these templates by
# default: ``['localtoc.html', 'relations.html', 'sourcelink.html',
# 'searchbox.html']``.
#
# html_sidebars = {}


# -- Options for HTMLHelp output ---------------------------------------------

# Output file base name for HTML help builder.
htmlhelp_basename = 'iceBladedoc'

