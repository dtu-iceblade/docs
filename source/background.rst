Background
==========

In cold climates, icing of the turbine blades can be a major issue when designing a wind farm. When ice grows on the turbine blades, it can lead to production losses, increased noise, and includes a risk of ice throw that could damage nearby property or lead to injury. Turbine manufacturers have developed ice prevention systems to help reduce these risks, but determining when to purchase them and for what turbines is still an open question. Additionally, ice forecasts combined with careful management of turbines can reduce the impact of icing in areas with lower ice impacts.  

IceBlade aims to fill this gap by estimating the amount of icing that a turbine will experience at pad level in terms of icing related production loss using meteorological modeling. The iceBlade model represents the turbine airfoil as a static cylinder, and accounts for both ice accretion and ice ablation. It has been used in previous studies :cite:`Davis2014,Davis2016a`. Some modification have been made in this version for ice ablation compared to the previous publications.

#. Ice accretion is modeled using the Makkonen model :cite:`Makkonen2000`. In this model, ice growth is mainly controlled by the amount of incoming liquid cloud water that impacts the turbine blade. The liquid cloud water depends on the incoming droplet velocity, the droplet size, and the size of the cylinder. 
#. Ice ablation sublimation, wind erosion, and total ice shedding are modelled. In contrast to the iceBlade version in :cite:`Davis2014,Davis2016a`, the following changes have been made: 
   
   #. Total ice shedding is triggered to remove all ice on the blade, when the model temperature is above 0 °C for more than 30 consecutive minutes. 
   #. Wind erosion depends on the velocity itself not the cubed velocity. The cubed velocity was initially used to represent the kinetic energy of particles, but found to be too difficult to fit for both cylinder and turbine simulations.


The iceBlade model is added into WRF and MPAS as a separate diagnostic module and includes a Registry 
file that allows for the runtime configuration of the model. The iceBlade model can be disabled, 
allowing the atmosphere model to be run as normal, by either not including the iceBlade namelist or 
deactivating the scheme. 
IceBlade is configured to run for 3 separate turbines and a cylinder for all model runs. 
Using a different number of turbines requires to modify the source code and re-compiling the WRF or MPAS model.

Postprocessing
--------------

IceBlade provides accumulated accretion and ablation over each output time step for a standard rotating cylinder as well as for the three turbine types (:ref:`see here <Turbine-file in WRF>` for WRF and :ref:`see here <Turbine-file in MPAS>` for MPAS)

.. code-block::

    ACCRE_CYL, ACCRE_BLD1, ACCRE_BLD2, ACCRE_BLD3
    ABLAT_CYL, ABLAT_BLD1, ABLAT_BLD2, ABLAT_BLD3

To derive the accumulated ice amount, ice accretion and ice ablation need to be combined:

.. code-block::

    import xarray as xr

    def ice_load(accre,ablat):
        load = accre.copy() * 0 
        for ihouroi,houroi in enumerate(accre.time):
            if houroi == accre.time.isel(time=0):
                acm = load.sel(time=houroi)
            else:
                acm = load.isel(time=ihouroi-1).copy()
            # Add ice when there is new accretion and remove when there is ablation
            acm += xr.where(accre.sel(time=houroi) > 0.0005, accre.sel(time=houroi), xr.where(ablat.sel(time=houroi) < 0., ablat.sel(time=houroi), 0.))
            # Ensure that ice never goes negative
            load.loc[{'time':houroi}] =  xr.where(acm < 0., 0., acm)
        return load

    load = ice_load(ds['ACCRE_CYL'].load(),ds['ABLAT_CYL'].load()).rename('ice_load')

Verification
------------
The iceBlade model in WRF has been validated in terms of annual icing loss against observations from 6 wind parks in Northern Sweden with satisfactory results in a confidential project with Vestas.

The iceBlade output of the here published version has been validated satisfactorily against measurements collected in the project ICE CONTROL (2016-2019) conducted by the Zentralanstalt fuer Meteorologie und Geodynamik (ZAMG), the University of Vienna, VERBUND Green Power GmbH, and Meteotest AB. ICE CONTROL was funded by the Austrian Research Promotion Agency (FFG) within the Energieforschungsprogramm des Klima- und Energiefonds, project 853575. Details are given :ref:`here <Verifying the iceBlade implementation>` and in the presentation :cite:`Fischereit2022`. It is recommended to reproduce this test case after implementing iceBlade to verify the correct implementation.

