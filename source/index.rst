.. iceBlade documentation master file, created by
   sphinx-quickstart on Tue Dec 15 16:30:59 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. .. tikz::
..    :include: logo.tikz

.. figure:: header_from_tikz.png
   :align: center


Welcome to iceBlade
====================================

The iceBlade model is a diagnostic module extension for the Weather, Research and Forecasting Model `WRF <https://github.com/wrf-model/WRF>`_
and the atmosphere component of the Model for Prediction Across Scales `(MPAS-A) <https://github.com/MPAS-Dev/MPAS-Model>`_
that models the turbine airfoil as a static cylinder, and accounts for both ice accretion and ice ablation.
Ice accretion is modeled using the Makkonen model. Ice ablation has models for sublimation, wind erosion,
and total ice shedding.

References can be found under :doc:`publications`.

..

License: Available under the `WRF <https://github.com/wrf-model/WRF/blob/master/LICENSE.txt>`_ and `MPAS <https://github.com/MPAS-Dev/MPAS-Model/blob/master/LICENSE>`_ public domain notice and disclaimer. DTU Wind is to be credited when iceBlade is used, distributed, adapted, build upon or otherwise applied.

Contents:
   .. toctree::
      :maxdepth: 2

      background
      installation
      installation-mpas
      verification
      publications


..
   Indices and tables
   ==================

   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`
