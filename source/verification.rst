Verifying the iceBlade implementation
=====================================

To verify the correct implementation of iceBlade measurements from the project ICE CONTROL (2016-2019) conducted by the Zentralanstalt fuer Meteorologie und Geodynamik (ZAMG), the University of Vienna, VERBUND Green Power GmbH, and Meteotest AB are provided :download:`here <ICE_CONTROL_measurement_data_Ellern_20170103_000000_20170105_000000.nc>`. ICE CONTROL was funded by the Austrian Research Promotion Agency (FFG) within the Energieforschungsprogramm des Klima- und Energiefonds, project 853575. Data have been published graphically in :cite:`Strauss2020`: Strauss, L., S. Serafin, and M. Dorninger, 2020: Skill and Potential Economic Value of Forecasts of Ice Accretion on Wind Turbines. J. Appl. Meteor. Climatol., 59, 1845-1864, https://doi.org/10.1175/JAMC-D-20-0025.1 and are reproduced in the figure below.

.. figure:: case_studies_scaled_new_complete.png
   :width: 400
   :align: center

   *Reproduced with modifications from* :cite:`Strauss2020`. *Thin bright blue and red lines indicate simulations performed in* :cite:`Strauss2020`, *while dots are measurements conducted in the project ICE CONTROL provided* :download:`here <ICE_CONTROL_measurement_data_Ellern_20170103_000000_20170105_000000.nc>`. *Thick dark blue and dark red lines show simulation results from WRF-iceBlade as described* :ref:`here <Reproduce the WRF-iceBlade simulation>` *and downloadable* :download:`here <ice_blade_output_ellern.nc>`. *Turquoise and orange lines show simulation results from MPAS-iceBlade as described*  :ref:`here <Reproduce the MPAS-iceBlade simulation>` *and downloadable* :download:`here <ts_mpas_ellern.nc>`. *For details regarding the different parameters see original publication* :cite:`Strauss2020`.

In WRF, the ice load from iceBlade in (k) generally agrees with the observations (blue dots), which lie between the simulated ice load on a rotating cylinder (solid line) and on the turbine blade (dotted line). The ice load on the rotating cylinder is slightly underestimated due to among others slightly higher simulated temperatures (a).

In MPAS, the ice load from iceBlade in (k) generally captures the timing of the icing event (blue dots). As in WRF the observations lie in between the ice load on a rotating cylinder (solid line) and on a turbine blade (dotted line). Since MPAS' non-aerosol aware Thompson scheme does not provide an estimate of the cloud water number concentration, Nc is not shown in panel (g) for MPAS.

Reproduce the WRF-iceBlade simulation
-------------------------------------
The iceBlade simulations are performed with WRF 4.2.2 using ERA5 (:cite:`era5`) and OSTIA (:cite:`Donlon2012`) as input and boundary data. The turbine configuration is shown :ref:`here <Turbine-file in WRF>`. The minimum and maximum rotational speeds correspond to the Enercon E-126 turbine in line with :cite:`Strauss2020` and values in between have been interpolated from those. Namelists to reproduce the simulations are given below:

* :download:`namelist-input <namelist.input>`
* :download:`wps-namelist <namelist.wps>`

Reproduce the MPAS-iceBlade simulation
--------------------------------------

The simulations have been performed with MPAS version 7.3 with the 6-hourly forecast product of the 
Climate Forecast System Version 2 (CFSv2) :cite:`CFSv2DataSet` as initial conditions. The underlying mesh 
for the simulation is based on a modified version of the 60-3km mesh (x20.835586) provided 
`here <https://www2.mmm.ucar.edu/projects/mpas/atmosphere_meshes/x20.835586.tar.gz>`_ where the center of the refinement zone 
has been rotated to 49.978 N, 7.6822 E (using the *grid_rotate* utility available 
`here <https://www2.mmm.ucar.edu/projects/mpas/atmosphere_meshes/x20.835586.tar.gz>`_). The namelist for the 
*init_atmosphere* and *atmosphere* model components as well as the list of zeta-levels used can be downloaded
here: 

* :download:`namelist.init_atmosphere <namelist.init_atmosphere>`
* :download:`namelist.atmosphere <namelist.atmosphere>`
* :download:`specified_zeta_levels.txt <specified_zeta_levels.txt>`